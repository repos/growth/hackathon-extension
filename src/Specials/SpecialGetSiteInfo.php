<?php

namespace MediaWiki\Extension\Hackathon\Specials;

use MediaWiki\Html\Html;
use MediaWiki\MainConfigNames;
use MediaWiki\SpecialPage\SpecialPage;

class SpecialGetSiteInfo extends SpecialPage {

	private const SITEINFO_TO_DISPLAY = [
		'hackathon-siteinfo-db-name' => MainConfigNames::DBname,
		'hackathon-siteinfo-db-username' => MainConfigNames::DBuser,
		'hackathon-siteinfo-ge-mentorship-enabled' => 'GEMentorshipEnabled',
	];

	public function __construct() {
		parent::__construct( 'GetSiteInfo' );
	}

	public function getDescription() {
		return $this->msg( 'hackathon-siteinfo-title' );
	}

	public function execute( $subPage ) {
		parent::execute( $subPage );

		$out = $this->getOutput();
		$out->addWikiMsg( 'hackathon-siteinfo-label' );

		$items = [];
		foreach ( self::SITEINFO_TO_DISPLAY as $msgKey => $configKey ) {
			$items[] = Html::rawElement(
				'li',
				[],
				$this->msg( $msgKey )
					->params( $this->getConfig()->get( $configKey ) )
					->parse()
			);
		}
		$out->addHTML( Html::rawElement( 'ul', [], implode( PHP_EOL, $items ) ) );
	}
}
