<?php
/**
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * @file
 */

namespace MediaWiki\Extension\Hackathon;

use MediaWiki\Hook\BeforePageDisplayHook;
use MediaWiki\Hook\SkinAddFooterLinksHook;
use MediaWiki\Html\Html;
use Skin;

class Hooks implements BeforePageDisplayHook, SkinAddFooterLinksHook {

	/**
	 * @inheritDoc
	 */
	public function onBeforePageDisplay( $out, $skin ): void {
		$config = $out->getConfig();
		$out->addInlineStyle(
			'.mw-page-container { background-color: '
				. $config->get( 'HackathonBackgroundColor' )
				. ' !important; }'
		);
	}

	/**
	 * @inheritDoc
	 */
	public function onSkinAddFooterLinks( Skin $skin, string $key, array &$footerItems ) {
		$link = $skin->getConfig()->get( 'HackathonReportBugsURL' );
		if ( $link ) {
			$footerItems['report-bug'] = Html::rawElement( 'a',
				[
					'href' => $link,
					'rel' => 'noreferrer noopener'
				],
				$skin->msg( 'hackathon-report-bugs-label' )->text()
			);
		}
	}
}
