This is an extension that changes `background-color` CSS property on all pages using the `BeforePageDisplay` hook.
